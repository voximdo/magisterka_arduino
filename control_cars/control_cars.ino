//Pins for controling track
#define o_control_track1 10

byte incomingByte;

void setup() {
  
 Serial.begin(115200);
 
 //Define pinModes
 pinMode(o_control_track1, OUTPUT);//Konfiguracja pinu jako wyjścia 
 
}
 
void loop() {
readSerial();
}

void readSerial()
{
   incomingByte = Serial.read();
 if (incomingByte == 255) return;   // exit the while(1), we're done receiving
 if (incomingByte == -1) return;  // if no characters are in the buffer read() returns -1
 
 if (incomingByte < 128) { //first track
  analogWrite(10, (2*incomingByte)+1);
 }

 
 Serial.println(incomingByte);
}

