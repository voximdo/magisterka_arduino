//define sensor pins, first four for track 1, second four for track 2
byte sensorPins[8] = {2,3,4,5,6,7,8,9};

//variables for statistic
int lapCount[2] = {0,0};
long sectorTimes[8] = {0,0,0,0,0,0,0,0};

//variables needed for calculations etc.
long sensorTimers[8] = {0,0,0,0,0,0,0,0};

int sensor1_1 = 2;
long t_s1_1 = 0;

//variables to setup
long sensorLag = 2000; //how much time have to pass till next catch (to avoid false readings)

int track1_lap_count = 0;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(115200);
  pinMode(11, OUTPUT);//Konfiguracja pinu jako wyjścia 
  analogWrite(11, 0);
  pinMode(10, OUTPUT);//Konfiguracja pinu jako wyjścia 
  analogWrite(10, 0);
  // make the pushbutton's pin an input:
  //pinMode(sensor1_1, INPUT);
    for (byte i = 0; i < 8; i++)
  {
    pinMode(sensorPins[i], INPUT);
  }
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input pin:

//  //function for checking 
//  if (digitalRead(sensor1_1) == HIGH)
//  {
//    if (millis() - t_s1_1 > sensor_lag)
//    {
//      Serial.println("Sensor 1_1 activated!");
//      track1_lap_count++;
//      Serial.println("Lap count: " + String(track1_lap_count));
//      t_s1_1 = millis();
//     }
//  }
//  else
//  {
//    //Serial.println("0");
//  }

  for (byte i = 0; i < 8; i++)
  {
    check_sensor(i);
  }
  
}

//Function for checking if car passed sensor
void check_sensor(byte sensorNumber) {
  if (digitalRead(sensorPins[sensorNumber]) == HIGH) {
    if (millis() - sensorTimers[sensorNumber] > sensorLag) {
      sensorTimers[sensorNumber] = millis();
       //send sector time through Serial monitor
      Serial.println(String(sensorNumber) + ";" + String(sensorTimers[sensorNumber]) + ";");
      //Check if car passed finish line
//      if(sensorNumber == 0) {
//        lapCount[0]++;
//        Serial.println("C0:" + String(lapCount[0])); //Send data about lap counts through Serial monitor
//      }
//      else if (sensorNumber == 4) {
//        lapCount[1]++;
//        Serial.println("C1:" + String(lapCount[1])); //Send data about lap counts through Serial monitor
//      }
    }
  }
}

//function for calculating times in 

