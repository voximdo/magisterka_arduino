//define sensor pins, first four for track 1, second four for track 2
byte sensorPins[8] = {2,3,4,5,6,7,8,9};

//variables for statistic
int lapCount[2] = {0,0};
long sectorTimes[8] = {0,0,0,0,0,0,0,0};

//variables needed for calculations etc.
long sensorTimers[8] = {0,0,0,0,0,0,0,0};

int sensor1_1 = 2;
long t_s1_1 = 0;

//variables to setup
long sensorLag = 2000; //how much time have to pass till next catch (to avoid false readings)

int track1_lap_count = 0;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(115200);
  pinMode(11, OUTPUT);//Konfiguracja pinu jako wyjścia 
  analogWrite(11, 122);
  pinMode(10, OUTPUT);//Konfiguracja pinu jako wyjścia 
  analogWrite(10, 0);
  // make the pushbutton's pin an input:
  //pinMode(sensor1_1, INPUT);
    for (byte i = 0; i < 8; i++)
  {
    pinMode(sensorPins[i], INPUT);
  }
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input pin:

  for (byte i = 0; i < 8; i++)
  {
    check_sensor(i);
  }
  
}

//Function for checking if car passed sensor
void check_sensor(byte sensorNumber) {
  if (digitalRead(sensorPins[sensorNumber]) == HIGH) {
    if (millis() - sensorTimers[sensorNumber] > sensorLag) {
      sensorTimers[sensorNumber] = millis();
      Serial.println(String(sensorNumber) + ";" + String(sensorTimers[sensorNumber]) + ";");
    }
  }
}

void readSerial()
{
   incomingByte = Serial.read();
 if (incomingByte == 255) return;   // exit the while(1), we're done receiving
 if (incomingByte == -1) return;  // if no characters are in the buffer read() returns -1
 
 if (incomingByte < 128) { //first track
  analogWrite(10, (2*incomingByte)+1);
 }
 
 else
 {
   incomingByte = incomingByte - 127;
   analogWrite(11, (2*incomingByte)+1);
 }

 
// Serial.println(incomingByte);
}

